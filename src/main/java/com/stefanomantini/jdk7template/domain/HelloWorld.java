package com.stefanomantini.jdk7template.domain;

public class HelloWorld {
    public long id;
    public String content;

    public HelloWorld(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "HelloWorld{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}
