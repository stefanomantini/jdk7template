package com.stefanomantini.jdk7template.api;

import com.stefanomantini.jdk7template.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.stefanomantini.jdk7template.repository.UserRepository;

import javax.validation.Valid;

@RestController
@RequestMapping(value="/api/")
public class UserApi {

    @Autowired
    UserRepository userRepository;

    @GetMapping("users")
    public Page<User> getAllPosts(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @PostMapping("users")
    public User createPost(@Valid @RequestBody User user) {
        return userRepository.save(user);
    }

    @PutMapping("users")
    public ResponseEntity<User> updatePost(@Valid @RequestBody User userRequest) {
        if (userRequest != null) {
            return ResponseEntity.badRequest().build();
        } else {
            User uMap = new User();
            uMap.setFirstName(userRequest.getFirstName());
            uMap.setLastName(userRequest.getLastName());
            uMap.setActivated(userRequest.getActivated());
            uMap.setLastModified(userRequest.getLastModified());
            User newUser = userRepository.save(uMap);
            return ResponseEntity.ok(newUser);
        }
    }
}
