package com.stefanomantini.jdk7template.configuration;

import com.google.common.base.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@ComponentScan("uk.gov.hmrc.writ.postcode")
public class SwaggerConfig {

    private final Logger log = LoggerFactory.getLogger(SwaggerConfig.class);

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("api")
                .apiInfo(apiMetadata())
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .build();
    }

    private ApiInfo apiMetadata() {
        return new ApiInfoBuilder()
                .title("Functional APIs required for the Postcode Service")
                .description("All the requests that the server will respond to.")
                .version("1.0.0")
                .build();
    }


    @Bean
    public Docket utilitiesDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("utility")
                .apiInfo(utilityMetadata())
                .select()
                .paths(utilityPaths())
                .build();
    }

    private ApiInfo utilityMetadata() {
        return new ApiInfoBuilder()
                .title("Utility APIs required for the Postcode Service")
                .description("All the requests that the server will respond to.")
                .version("1.0.0")
                .build();
    }

    private Predicate<String> utilityPaths() {
        return or(
                regex("/error.*")
                , regex("/actuator.*")
        );
    }


}

