package com.stefanomantini.jdk7template.repository;

import com.stefanomantini.jdk7template.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Page<User> findById(Long userId, Pageable pageable);
}
