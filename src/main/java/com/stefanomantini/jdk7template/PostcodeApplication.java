package com.stefanomantini.jdk7template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static com.stefanomantini.jdk7template.configuration.Constants.SPRING_PROFILE_DEVELOPMENT;

@SpringBootApplication
public class PostcodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostcodeApplication.class, args);
    }

    @Configuration
    @Profile(SPRING_PROFILE_DEVELOPMENT)
    @ComponentScan(lazyInit = true)
    static class LocalConfig {
    }
}
