package com.stefanomantini.jdk7template.integration.definitions;

import com.stefanomantini.jdk7template.PostcodeApplication;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import com.stefanomantini.jdk7template.configuration.Constants;

@SpringBootTest(classes = PostcodeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(Constants.INTEGRATION_TEST)
@ContextConfiguration
public class CucumberRoot {

    @Autowired
    protected TestRestTemplate template;

    @Before
    public void before() {} // do something globally

}
